
export interface Message {
    id: string;

    userId: string;

    chatId: string;

    authorName: string;

    text: string;
}