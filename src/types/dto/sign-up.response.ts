import {User} from "../entities/user.entity";

export interface SignUpResponse {
    jwtToken: string;
    user: User;
}