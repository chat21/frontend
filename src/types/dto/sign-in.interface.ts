import {SignUpDto} from "./sign-up.interface";

export interface SignInDto extends SignUpDto {}