import {BrowserRouter, Route, Routes} from "react-router-dom";
import Auth from "./pages/auth/auth";
import {Chat} from "./pages/chat/chat";

function App() {
  return (
      <BrowserRouter>
      <Routes>
          <Route path={'/'} element={<Auth />}></Route>
          <Route path={'/chat'} element={<Chat/>}></Route>
      </Routes>
      </BrowserRouter>
  )
}

export default App;
