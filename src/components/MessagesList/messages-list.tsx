import './index.css';
import {Message} from "../../types/entities/message.entity";

interface Props {
    messages: Message[];
}

export function MessagesList({messages}: Props) {
    if (!messages) {
        return null;
    }

    return (
        <div>
            {messages?.map((message) => (
                <div key={message.id}>
                    <p>
                        <span>{message.authorName}: {message.text}</span>
                    </p>
                </div>
            ))}
        </div>
    )
}