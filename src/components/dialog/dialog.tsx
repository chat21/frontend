import {MessagesList} from "../MessagesList";
import './index.css';
import React, {useState} from "react";
import {Message} from "../../types/entities/message.entity";

interface Props {
    title: string;
    messages: Message[];
    sendMessage: any;
}

export function Dialog({title, messages, sendMessage}: Props) {
    const [newMessage, setNewMessage] = useState("");
    if (!title) {
        return null;
    }

    const handleClickSubmit = (e) => {
        e.preventDefault();
        sendMessage(newMessage);
        setNewMessage('');
    }

    return (
        <div>
            <div className="dialog-body">
            <p className='dialog-title'>{title}</p>
            <div className="dialog-messages"><MessagesList messages={messages}></MessagesList></div>
            </div>
            <div className='dialog-msg-input'>
            <input className='dialog-input' onChange={(e) => setNewMessage(e.target.value)} value={newMessage}/>
            <button className='dialog-submit' type='submit' onClick={handleClickSubmit}>Send</button>
            </div>
        </div>
    )
}