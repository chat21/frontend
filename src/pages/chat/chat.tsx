import React, {useEffect, useRef, useState} from 'react';
import { Dialog } from "../../components/dialog/dialog";
import io from 'socket.io-client';
import { IChat } from "../../types/entities/chat.interface";
import { Message } from "../../types/entities/message.entity";
import "./index.css"

export function Chat() {
    const socket = useRef(null);
    const [chats, setChats] = useState<IChat[]>([]);
    const [currentChat, setCurrentChat] = useState<IChat>(null);
    const [messages, setMessages] = useState<Message[]>([]);

    const token = localStorage.getItem('token');
    if (!token) {
        window.location.pathname = '/';
    }

    useEffect(() => {
        const socketOptions = {
            query: {
                    bearerToken: token
                },
            };
        socket.current = io('https://unichatroulette.herokuapp.com/', socketOptions);

        socket.current.emit('getChats');

        socket.current.on('chatsToClient', (chats: IChat[]) => setChats(chats));

        socket.current.on('chatToClient', (chat: IChat) => {
            setChats(prevChats => [...prevChats.filter(c => c.id !== chat.id), chat]);
            handleJoinChat(chat);
        })

        socket.current.on('messageToChat', (message: Message) => setMessages(prev => [...prev, message]));

        socket.current.on('messagesToChat', (messages: Message[]) => setMessages(messages));

        return () => socket.current.disconnect();
    }, [])

    const handleSendMessage = (message: string) => {
        socket.current.emit('sendMessage', {
            chatId: currentChat.id,
            text: message,
        });
    }

    const handleFindChat = () => {
        socket.current.emit('createChat');
    }

    const handleJoinChat = (chat: IChat) => {
        setCurrentChat(chat);
        socket.current.emit('joinChat', { chatId: chat.id });
    }

    return (
        <div className="chatting-space">
            <div className="chats-list-with-btn">
                <div className='chats-list-wrapper'>
                    {chats?.map((chat) => (
                        <div key={chat.id} onClick={() => handleJoinChat(chat)}>
                            <p>{chat.title}</p>
                        </div>
                    ))}
                </div>
                <button className="find-chat" type='submit' onClick={handleFindChat}>Find Chat</button>
            </div>
            <div className="dialog">
                <Dialog messages={messages} sendMessage={handleSendMessage} {...currentChat}></Dialog>
            </div>
        </div>
    )
}