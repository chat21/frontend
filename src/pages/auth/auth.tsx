import React, { useState} from 'react';
import './index.css';
import {SignInUser, SignUpUser} from "../../api";
import '../../styles.css'

export default function Auth() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const handleSignIn = async (e: React.MouseEvent | React.FormEvent) => {
        e.preventDefault();
        try {
            const {jwtToken} = await SignInUser({username, password});
            localStorage.setItem('token', jwtToken);
            window.location.pathname = '/chat';
        } catch (e: any) {
            alert(e.message);
        }
    }

    const handleSignUp = async (e: React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        try {
            const {jwtToken, user} = await SignUpUser({username, password});
            localStorage.setItem('token', jwtToken);
            localStorage.setItem('user', JSON.stringify(user));
            window.location.pathname = '/chat';
        } catch (e: any) {
            alert(e.message);
        }
    }

    return(
        <div className="login-wrapper">
            <h1 className="greeting-message">Please Log In</h1>
            <form className="auth-form" onSubmit={handleSignIn}>
                <label>
                    <p>Username</p>
                    <input className="username" type="text" onChange={e => setUsername(e.target.value)}/>
                </label>
                <label>
                    <p>Password</p>
                    <input className="password" type="password" onChange={e => setPassword(e.target.value)}/>
                </label>
                <div className="auth-btns">
                    <button className="sign-in" type="submit" onClick={handleSignIn}>SignIn</button>
                    <button className="sign-up" type="submit" onClick={handleSignUp}>SignUp</button>
                </div>
            </form>
        </div>
    )
}