import {SignInDto} from "../types/dto/sign-in.interface";
import {SignInResponse} from "../types/dto/sign-in.response";
import {SignUpDto} from "../types/dto/sign-up.interface";
import {SignUpResponse} from "../types/dto/sign-up.response";

const API_URL = process.env.REACT_APP_API_URL;
const API_AUTH_URL = 'auth/';

export async function SignInUser(signInDto: SignInDto): Promise<SignInResponse> {
    const res = await fetch(API_URL + API_AUTH_URL + 'signIn', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(signInDto)
    });
    if (res.ok) {
        const jsonRes = await res.json();
        return jsonRes as SignInResponse;
    } else {
        throw new Error('Ошибка авторизации');
    }
}

export async function SignUpUser(signUpDto: SignUpDto): Promise<SignUpResponse> {
    const res = await fetch(API_URL + API_AUTH_URL + 'signUp', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(signUpDto)
    });
    if (res.ok) {
        const resJson = await res.json();
        return resJson as SignUpResponse;
    } else {
        throw new Error('Ошибка регистрации');
    }
}